# Instruções para SD2018-TP2

#TO-DO:
* [x] Kafka discovery 
* [x] Datanode on Mongo
* [x] Replicar Datanode em anel, por nos dois a frente. consistencia.
* [ ] Datanode Proxy with Dropbox
* [ ] Multiplos Namenodes, replicar dados, consistencia.
* [x] REST on SSL 
* [ ] Relatório

#Melhorias possiveis:
* [ ] Adicionar outro WebService? Análise de sentimentos..
* [ ] Autenticar agentes MapReduce.

#Comandos Importantes e Notas
O pom foi alterado para copiar certificados do sitio correto.
##Como testar usando docker
1. Correr script 
2. Usar maven para gerar imagem docker
3. Correr imagem docker interactivamente
4. La dentro lançar o jar 
```
./sd18_services.sh
mvn install
docker run --network="sd-net" -ti sd18-tp1-smd /bin/bash
java -cp /home/sd/*.jar tests.KafkaDiscoveryTest
```

##Argumentos VM:
Melhor ainda é programaticamente definir estes parametros.
###Cliente:
-Djavax.net.ssl.keyStore=storage-client.jks
-Djavax.net.ssl.keyStorePassword=SD2018

###Servidor:
-Djavax.net.ssl.keyStore=./datanode-server.jks
-Djavax.net.ssl.keyStorePassword=SD2018


###Gerar Par Pub/Priv: 
```
keytool -genkey -alias namenode-server -keyalg RSA -validity 365 -keystore ./namenode-server.jks -file namenode-server.cert
```

###Gerar Certificado apartir de par Pub/Priv em JKS: 
```
keytool -exportcert -alias namenode-server -keystore namenode-server.jks -file namenode-server.cert
```

###Importar Certificado para JKS do Cliente: 
```
keytool -importcert -file datanode-server.cert -alias datanode-server -keystore storage-client.jks
```
###Password dos JKS: 
```
SD2018
```

