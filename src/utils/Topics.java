package utils;

import java.util.Properties;

import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import org.apache.kafka.common.errors.TopicExistsException;

import kafka.admin.AdminUtils;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;

public class Topics {

	private static final int SESSION_TIMEOUT = 5000;
	private static final int CONNECTION_TIMEOUT = 1000;
	private static final String ZOOKEEPER_SERVER = "zoo1:2181,zoo2:2181,zoo3:2181";

	private static final int REPLICATION_FACTOR = 1;

	
	public static void createTopic( String topic, int numPartitions ) {
		try {
			ZkClient zkClient = new ZkClient(
		            ZOOKEEPER_SERVER,
		            SESSION_TIMEOUT,
		            CONNECTION_TIMEOUT,
		            ZKStringSerializer$.MODULE$);
			
			ZkUtils zkUtils = new ZkUtils(zkClient, new ZkConnection(ZOOKEEPER_SERVER), false);
	
			
			Properties topicConfig = new Properties();
			AdminUtils.createTopic(zkUtils, topic, numPartitions, REPLICATION_FACTOR, topicConfig, null);		
		} catch( TopicExistsException e ) {		
			System.err.println("Topic already exists...");
		}
	}
	
}
