package utils;

import java.net.URI;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

import utils.JKS;

@SuppressWarnings("restriction")
public class HTTPSAuthenticatingServer {
	HttpsServer server;
	public HTTPSAuthenticatingServer(String serverKeystore, String serverKeystorePWD,String serverTrustStore,String serverTruststorePWD, String uriBase, ResourceConfig config)throws Exception {
		List<X509Certificate> trustedCertificates = new ArrayList<>();

		KeyStore ks = JKS.load(serverKeystore, serverKeystorePWD);
		KeyStore ts = JKS.load(serverTrustStore, serverTruststorePWD);

		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(ks, serverKeystorePWD.toCharArray());

		SSLContext ctx = SSLContext.getInstance("TLS");

		TrustManagerFactory tmf2 = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf2.init(ts);

		for (TrustManager tm : tmf2.getTrustManagers()) {
			if (tm instanceof X509TrustManager)
				trustedCertificates.addAll(Arrays.asList(((X509TrustManager) tm).getAcceptedIssuers()));
		}

		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {

			@Override
			public void checkClientTrusted(X509Certificate[] certs, String authType) {
				System.err.println(certs[0].getSubjectX500Principal());
			}

			@Override
			public void checkServerTrusted(X509Certificate[] certs, String authType) {
				Thread.dumpStack();
			}

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return trustedCertificates.toArray(new X509Certificate[0]);
			}
		} };


		ctx.init(kmf.getKeyManagers(), trustAllCerts, null);

		HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());

		

		server = (HttpsServer) JdkHttpServerFactory.createHttpServer(URI.create(uriBase), config, ctx,
				false);

		server.setHttpsConfigurator(new HttpsConfigurator(ctx) {
			@Override
			public void configure(HttpsParameters params) {
				SSLParameters sslparams = ctx.getDefaultSSLParameters();
				sslparams.setNeedClientAuth(true);
				params.setSSLParameters(sslparams);
			}
		});

		
	}
	public void start() {
		server.start();
	}

}
