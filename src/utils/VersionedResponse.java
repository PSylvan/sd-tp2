package utils;

import javax.ws.rs.core.Response;

public class VersionedResponse {

	public static int getVersion(Response r) {
		if (r.getHeaders().containsKey("version"))
			return Integer.parseInt(r.getHeaderString("version"));
		return 0;
	}
}
