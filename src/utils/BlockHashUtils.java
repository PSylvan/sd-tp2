package utils;

public class BlockHashUtils {

	private static final String DELIMITER = " ";

	public static boolean checkHash(byte[] data) {
		String dataWithChecksum = new String(data);
		String storedChecksum = dataWithChecksum.split(DELIMITER)[0];
		String blockData = dataWithChecksum.substring(storedChecksum.length() + 1);
		String newChecksum = Base58.encode((Hash.sha256(blockData.getBytes())));
		return newChecksum.equals(storedChecksum);
	}

	public static byte[] removeHash(byte[] data) {
		String dataWithChecksum = new String(data);
		String storedChecksum = dataWithChecksum.split(DELIMITER)[0];
		String blockData = dataWithChecksum.substring(storedChecksum.length() + 1);
		return blockData.getBytes();
	}
}
