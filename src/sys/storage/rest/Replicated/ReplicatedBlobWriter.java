package sys.storage.rest.Replicated;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import javax.ws.rs.core.Response;

import api.storage.Datanode;
import api.storage.Namenode;
import sys.storage.rest.RestBlobStorage;
import sys.storage.rest.RestDatanodeClient;
import sys.storage.rest.RestNamenodeClient;
import utils.Base58;
import utils.Hash;
import utils.IO;
import utils.Random;
import utils.VersionedResponse;
import api.storage.BlobStorage.BlobWriter;

public class ReplicatedBlobWriter implements BlobWriter {

	final String name;
	final int blockSize;
	final ByteArrayOutputStream buf;

	final Map<URI, Long> namenodes;
	final Map<URI, Long> datanodes;// ordered by uri
	final List<String> blocks = new LinkedList<String>();
	final BiFunction<String, Integer, Integer> storagePolicy;
	AtomicInteger minVersion;

	public ReplicatedBlobWriter(String name, Map<URI, Long> namenodes, Map<URI, Long> datanodes, int blockSize,
			BiFunction<String, Integer, Integer> blockStoragePolicy, AtomicInteger minVersion) {

		this.name = name;
		this.namenodes = namenodes;
		this.datanodes = datanodes;

		this.blockSize = blockSize;
		this.buf = new ByteArrayOutputStream(blockSize);
		this.storagePolicy = blockStoragePolicy;
		this.minVersion = minVersion;
	}

	// selects the datanode based on the storage policy (uses the name and index of
	// the block).
	private int selectDatanode(int datanodeSize) {
		return storagePolicy.apply(name, blocks.size()) % datanodeSize;
	}

	private void flush(boolean eob) {
		if (buf.size() > 0) {
			String blockEntry = writeInDatanodes();
			blocks.add(blockEntry);
		}
		if (eob && blocks.size() > 0) {
			createBlobNamenode();
			blocks.clear();
		}

		buf.reset();
	}

	private void createBlobNamenode() {
		Response r = null;
		URI[] uris = namenodes.keySet().toArray(new URI[0]);
		Namenode namenode;
		while (r == null) {
			namenode = new RestNamenodeClient(uris[Random.nextInt(uris.length)]);
			r = namenode.create(name, blocks);// Try a random namenode, any will
		} // do
		minVersion.set(VersionedResponse.getVersion(r));
	}

	private String writeInDatanodes() {
		String blockEntry = "";
		URI[] uris = datanodes.keySet().toArray(new URI[0]);
		blockEntry = blockEntry.concat(Base58.encode(Hash.sha256(buf.toByteArray())) + RestBlobStorage.DELIMITER);// save the hash
		int datanodeChosen = selectDatanode(uris.length);
		int numReplicas = Math.min(uris.length, RestBlobStorage.REPLICATION_FACTOR);
		int datanodeIndexOffset = 0;
		Datanode datanode;
		for (int i = 0; i < numReplicas; i++) {
			datanode = new RestDatanodeClient(uris[(datanodeChosen + datanodeIndexOffset++) % uris.length]);
			String uri = datanode.createBlock(buf.toByteArray(), name);
			if (!uri.equals(RestDatanodeClient.FAILED)) {
				blockEntry = blockEntry.concat(uri + RestBlobStorage.DELIMITER);
			} else {
				i--;
				if (datanodeIndexOffset > 3 * uris.length)
					break; // wrote as much as possible, but not enough
			}

		}
		return blockEntry;
	}

	@Override
	public void writeLine(String line) {
		if (buf.size() + line.length() > blockSize - 1) {
			this.flush(false);
		}
		IO.write(buf, line.getBytes());
		IO.write(buf, '\n');
	}

	@Override
	public void close() {
		flush(true);
	}
}
