package sys.storage.rest.Replicated;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.Trie;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

public class KafkaReplicatedNamenodeConsumer extends Thread {
	private Trie<String, List<String>> names;
	private Map<String, SynchronousQueue<Status>> requestToQueue;
	 AtomicInteger offset;
	 
	 public KafkaReplicatedNamenodeConsumer(Trie<String, List<String>> names, Map<String, SynchronousQueue<Status>> requestToQueue, AtomicInteger offset) {
		 this.names = names;
			this.requestToQueue = requestToQueue;
			this.offset = offset;
	 }
	public void run() {
		kafkaConsumer();
	}
	
	private void kafkaConsumer() {
		Properties cProps = new Properties();
		cProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); 
		cProps.put(ConsumerConfig.GROUP_ID_CONFIG, UUID.randomUUID().toString()); // Generate uniqueid so we get offset from
																		// beggining.
		cProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka1:9092,kafka2:9092,kafka3:9092");
		cProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringDeserializer");
		cProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringDeserializer");
		Consumer<String, String> consumer = new KafkaConsumer<>(cProps);

		consumer.subscribe(Arrays.asList(ReplicatedNamenodeResources.NAMENODE_CONSISTENCY));// subscrever ao topico "Namenode_Log"
		 consumer.poll(1);//dummy poll
		consumer.seek(new TopicPartition(ReplicatedNamenodeResources.NAMENODE_CONSISTENCY, 0), offset.get()); // Colocar o offset correto
		while (true) {
			ConsumerRecords<String, String> records = consumer.poll(Integer.MAX_VALUE);
			if(records.count() > 0) {
			records.forEach(r -> {
				String msg = r.value().trim();
				String[] msgTokens = r.value().split(" ");
				String action = msgTokens[1];
				try {
					Status result;
					switch (action) {
					case "create":
						result = _create(msgTokens[2], Arrays.asList(msgTokens).subList(3, msgTokens.length));
						if (requestToQueue.containsKey(msg)) {
							requestToQueue.get(msg).put(result);
						}

						break;
					case "delete":
						result = _delete(msgTokens[2]);
						if (requestToQueue.containsKey(r.value()))
							requestToQueue.get(r.value()).put(result);
						break;

					case "update":
						result = _update(msgTokens[2], Arrays.asList(msgTokens).subList(3, msgTokens.length));
						if (requestToQueue.containsKey(r.value()))
							requestToQueue.get(r.value()).put(result);
						break;
					}

				} catch (InterruptedException e) {
					System.err.println("INTERRUPTED EXCEPTION!!!");
				}
				offset.set((int)r.offset());
			});
			}
		}
	}
	
	synchronized private Status _create(String name, List<String> metadata) {
		if (names.putIfAbsent(name, metadata) != null)
			return Status.CONFLICT;
		return Status.NO_CONTENT; // Everything OK 204
	}

	synchronized private Status _delete(String prefix) {
		Set<String> keys = names.prefixMap(prefix).keySet();
		if (!keys.isEmpty())
			names.keySet().removeAll(new ArrayList<>(keys));
		else
			return Status.NOT_FOUND;
		return Status.NO_CONTENT;// Everything OK 204
	}

	synchronized private Status _update(String name, List<String> metadata) {
		if (names.putIfAbsent(name, metadata) == null)
			return Status.NOT_FOUND;
		return Status.NO_CONTENT;// Everything OK 204
	}
}
