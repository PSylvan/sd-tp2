package sys.storage.rest.Replicated;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.Trie;
import org.apache.commons.collections4.trie.PatriciaTrie;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import api.storage.Namenode;
import utils.IO;
import utils.JSON;
import utils.Sleep;
import utils.Topics;

public class ReplicatedNamenodeResources implements Namenode {
	static final int NAMENODE_PORT = 7777;
	public static final String NAMENODE_CONSISTENCY = "Namenode_Log";
	private static Logger logger = Logger.getLogger(Namenode.class.toString());
	private Trie<String, List<String>> names;
	private Producer<String, String> producer;
	private Map<String, SynchronousQueue<Status>> requestToQueue;
	private AtomicInteger offset; // Is the offset of the next message we want to read.

	public ReplicatedNamenodeResources() {
		System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "OFF");

		Properties pProps = new Properties();
		pProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka1:9092,kafka2:9092,kafka3:9092");
		pProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		pProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		try {
			TriePlusOffset tmp = readDSFromFile();
			names = tmp.getNames();
			offset = new AtomicInteger(tmp.getOffset());
		} catch (Exception e) {
			names = new PatriciaTrie<>();
			offset = new AtomicInteger(0);
		}
		requestToQueue = new ConcurrentHashMap<String, SynchronousQueue<Status>>();
		Topics.createTopic(NAMENODE_CONSISTENCY, 1);
		producer = new KafkaProducer<>(pProps);

		new Thread(this::takeSnapshotOfDS).start();

		Thread consumer = new KafkaReplicatedNamenodeConsumer(names, requestToQueue, offset);
		consumer.start();
	}

	private TriePlusOffset readDSFromFile() throws FileNotFoundException, IOException, ClassNotFoundException {
		File stored = new File("kafkaNamenodeResources.ds");
		TriePlusOffset tpo = null;
		if (stored.exists()) {
			String read = new String(IO.read(stored));
			tpo = JSON.decode(read, TriePlusOffset.class);
		}

		return tpo;

	}

	private void takeSnapshotOfDS() {
		final int RETRY_PERIOD = 10000;
		File stored = new File("kafkaNamenodeResources.ds");
		while (true) {
			IO.write(stored, JSON.encode(new TriePlusOffset(names, offset.get())).getBytes());
			Sleep.ms(RETRY_PERIOD);
		}
	}

	private Status sendKafka(String[] data) {
		String msg = "";
		msg = msg.concat(System.currentTimeMillis() + " "); // Timestamp is included to generate unique hashcodes
		for (int i = 0; i < data.length; i++)
			msg = msg.concat(data[i] + " ");
		msg = msg.trim();
		requestToQueue.put(msg, new SynchronousQueue<Status>());
		producer.send(new ProducerRecord<String, String>(NAMENODE_CONSISTENCY, msg));
		Status resp = null;
		try {
			resp = requestToQueue.get(msg).take();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		requestToQueue.remove(msg);
		return resp;

	}

	@Override
	synchronized public Response list(String prefix, int version)  {
		logger.fine("Namenode: listing the prefix " + prefix + " with the version " + version);
		while (offset.get() < version)
			;
		return Response.ok(new ArrayList<>(names.prefixMap(prefix).keySet())).header("version", offset.get()).build();
	}

	@Override
	synchronized public Response create(String name, List<String> metadata) {
		logger.fine("Namenode: creating blob " + name + " with " + metadata.size() + " blocks");
		metadata.add(0, name);
		metadata.add(0, "create");

		Status resp = sendKafka(metadata.toArray(new String[metadata.size()]));
		if (resp == Status.CONFLICT)
			throw new WebApplicationException(Status.CONFLICT);
		return Response.status(Status.NO_CONTENT).header("version", offset.get()).build();
	}

	@Override
	synchronized public Response delete(String prefix) {
		logger.fine("Namenode: deleting the prefix " + prefix);
		Status resp = sendKafka(new String[] { "delete", prefix });
		if (resp == Status.NOT_FOUND)
			throw new WebApplicationException(Status.NOT_FOUND);
		return Response.status(Status.NO_CONTENT).header("version", offset.get()).build();
	}

	@Override
	synchronized public Response update(String name, List<String> metadata) {
		logger.fine("Namenode: updating blob " + name + " with " + metadata.size() + "blocks");

		metadata.add(0, name);
		metadata.add(0, "update");
		Status resp = sendKafka(metadata.toArray(new String[metadata.size()]));
		if (resp == Status.NOT_FOUND)
			throw new WebApplicationException(Status.NOT_FOUND);
		return Response.status(Status.NO_CONTENT).header("version", offset.get()).build();
	}

	@Override
	synchronized public Response read(String name, int version) {
		logger.fine("Namenode: Reading blob " + name + " with the version " + version);
		while (offset.get() < version)
			;
		List<String> metadata = names.get(name);
		if (metadata == null)
			throw new WebApplicationException(Status.NOT_FOUND);
		return Response.ok(metadata).header("version", offset.get()).build();
	}

	private class TriePlusOffset implements Serializable {
		private static final long serialVersionUID = -3770623632844321169L;
		public int offset;
		public Trie<String, List<String>> names;

		public TriePlusOffset(Trie<String, List<String>> names, int offset) {
			this.names = names;
			this.offset = offset;
		}

		public int getOffset() {
			return offset;
		}

		public Trie<String, List<String>> getNames() {
			return names;
		}

	}

}
