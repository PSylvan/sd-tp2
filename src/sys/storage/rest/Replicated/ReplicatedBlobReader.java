package sys.storage.rest.Replicated;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import api.storage.Datanode;
import api.storage.Namenode;
import sys.storage.rest.RestBlobStorage;
import sys.storage.rest.RestNamenodeClient;
import utils.Base58;
import utils.Hash;
import utils.VersionedResponse;
import api.storage.BlobStorage.BlobReader;

public class ReplicatedBlobReader implements BlobReader {
	
	final String name;
	final  Map<URI, Long> namenodes;
	final Datanode datanode;

	Iterator<String> blocks;
	AtomicInteger minVersion;

	final SmartBlockReader smartBlockIterator;

	public ReplicatedBlobReader(String name,  Map<URI, Long> namenodes, Datanode datanode, AtomicInteger minVersion) {
		this.name = name;
		this.namenodes = namenodes;
		this.datanode = datanode;
		this.minVersion = minVersion;
		this.blocks = null;
		int i = 0;
		URI[] uris = namenodes.keySet().toArray(new URI[0]);
		Namenode namenode;
		while (blocks == null) {
			namenode = new RestNamenodeClient(uris[i++]);
			Response r = namenode.read(name, minVersion.get());
			if (r != null) {
	
				this.blocks = r.readEntity(new GenericType<List<String>>() {
				}).iterator();
			
				minVersion.set(VersionedResponse.getVersion(r));
			}
		}
		this.smartBlockIterator = new SmartBlockReader();
	}

	@Override
	public String readLine(){
		return smartBlockIterator.hasNext() ? smartBlockIterator.next() :  null;
	}

	@Override
	public Iterator<String> iterator() {
		return smartBlockIterator;
	}

	private Iterator<String> nextBlockLines() {
		if (blocks.hasNext())
			return fetchBlockLines(blocks.next()).iterator();
		else
			return Collections.emptyIterator();
	}

	private List<String> fetchBlockLines(String blockEntry) {
		byte[] data = null;
		String[] tokens = blockEntry.split(RestBlobStorage.DELIMITER);
		boolean isCorrupted = true;
		String storedHash = tokens[0];
		for (int i = 1; i < tokens.length; i++) {
			data = datanode.readBlock(tokens[i]); // if no response, ask next guy
			if (data != null) {
				String computedHash = Base58.encode(Hash.sha256(data));
				if (storedHash.equals(computedHash)) {
					isCorrupted = false;
					break;
				}
			}
		}
		if (isCorrupted)
			data = "".getBytes();
		return Arrays.asList(new String(data).split("\\R"));
	}

	private class SmartBlockReader implements Iterator<String> {

		Iterator<String> lines;

		SmartBlockReader() {
			this.lines = nextBlockLines();
		}

		@Override
		public String next() {
			return lines.next();
		}

		@Override
		public boolean hasNext() {
			return lines.hasNext() || (lines = nextBlockLines()).hasNext();
		}
	}

}
