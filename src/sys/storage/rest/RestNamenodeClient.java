package sys.storage.rest;

import static sys.storage.rest.namenode.NamenodeServer.NAMENODE;

import java.net.URI;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import api.storage.Namenode;
import discovery.Discover;

public class RestNamenodeClient extends RestClient implements Namenode {

	public RestNamenodeClient() {
		this(Discover.uriOf(NAMENODE));
	}

	public RestNamenodeClient(URI namenode) {
		super(namenode, Namenode.PATH);
	}

	@Override
	public Response list(String prefix, int version) {
		return reTryNTimes(() -> _list(prefix, version), 3);
	}

	@Override
	public Response read(String name, int version) {
		return reTryNTimes(() -> _read(name, version), 3);
	}

	@Override
	public Response create(String name, List<String> metadata) {
		return reTryNTimes(() -> _create(name, metadata), 3);
	}

	@Override
	public Response update(String name, List<String> metadata) {
		return reTryNTimes(() -> _update(name, metadata), 3);
	}

	@Override
	public Response delete(String prefix) {

		return reTryNTimes(() -> _delete(prefix), 3);
	}

	private Response _list(String prefix, int version) {
		Response r = target.path("list").queryParam("prefix", prefix).queryParam("version", version).request()
				.accept(MediaType.APPLICATION_JSON).get();
		verifyResponse(Status.OK, r);
		return r;

	}

	private Response _read(String name, int version) {
		Response r = target.path(name).queryParam("version", version).request().accept(MediaType.APPLICATION_JSON)
				.get();
		verifyResponse(Status.OK, r);
		return r;

	}

	private Response _create(String name, List<String> metadata) {
		Response r = target.path(name).request().post(Entity.entity(metadata, MediaType.APPLICATION_JSON));

		verifyResponse(Status.NO_CONTENT, r);
		return r;
	}

	private Response _update(String name, List<String> metadata) {
		Response r = target.path(name).request().put(Entity.entity(metadata, MediaType.APPLICATION_JSON));

		verifyResponse(Status.NO_CONTENT, r);
		return r;
	}

	private Response _delete(String prefix) {
		Response r = target.path("list").queryParam("prefix", prefix).request().delete();
		verifyResponse(Status.NO_CONTENT, r);
		return r;

	}
}
