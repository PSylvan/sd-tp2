package sys.storage.rest.namenode;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.Trie;
import org.apache.commons.collections4.trie.PatriciaTrie;

import api.storage.Namenode;

public class NamenodeResources implements Namenode {

	private Trie<String, List<String>> names = new PatriciaTrie<>();
	
	@Override
	synchronized public Response list(String prefix, int version) {
		return Response.ok(new ArrayList<>(names.prefixMap( prefix ).keySet())).build();
	}

	@Override
	synchronized public Response create(String name,  List<String> metadata) {
		System.err.println("Create:"+metadata.get(0));
		
		
		if( names.putIfAbsent(name, metadata) != null )
			throw new WebApplicationException( Status.CONFLICT );
		return Response.status(Status.NO_CONTENT).build();
	}

	@Override
	synchronized public Response delete(String prefix) {
		Set<String> keys = names.prefixMap( prefix ).keySet();
		if( ! keys.isEmpty() )
			names.keySet().removeAll( new ArrayList<>(keys) );
		else
			throw new WebApplicationException( Status.NOT_FOUND );
		return Response.status(Status.NO_CONTENT).build();
	}

	@Override
	synchronized public Response update(String name, List<String> metadata) {
		if( names.putIfAbsent( name, metadata) == null )
			throw new WebApplicationException( Status.NOT_FOUND );
		return Response.status(Status.NO_CONTENT).build();
		
	}

	@Override
	synchronized public Response read(String name, int version) {
		List<String> metadata = names.get( name );
		if( metadata == null )
			throw new WebApplicationException( Status.NOT_FOUND );
		else
			return Response.ok(metadata).build();
	}

	
}
