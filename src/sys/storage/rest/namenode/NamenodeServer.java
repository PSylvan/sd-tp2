package sys.storage.rest.namenode;

import static utils.Log.Log;

import java.util.logging.Level;

import org.glassfish.jersey.server.ResourceConfig;

import api.storage.Namenode;
import discovery.Discover;
import sys.storage.rest.RestBlobStorage;
import sys.storage.rest.Replicated.ReplicatedNamenodeResources;
import utils.HTTPSAuthenticatingServer;
import utils.IP;

public class NamenodeServer {
	private static final String SERVER_KEYSTORE = "/home/sd/namenode-server-keystore.jks";
	private static final String SERVER_KEYSTORE_PWD = "SD2018";

	private static final String SERVER_TRUSTSTORE = "/home/sd/namenode-truststore.jks";
	private static final String SERVER_TRUSTSTORE_PWD = "SD2018";
	public static final String NAMENODE = "Namenode";

	public static final int NAMENODE_PORT = 7777;

	public static void main(String[] args) throws Exception {
	
		System.setProperty("java.net.preferIPv4Stack", "true");
		if (RestBlobStorage.LOCAL_TESTING) {
			System.setProperty("javax.net.ssl.keyStore", "./tls/namenode-server-keystore.jks");
			System.setProperty("javax.net.ssl.keyStorePassword", "SD2018");
			System.setProperty("javax.net.ssl.trustStore", "./tls/namenode-truststore-with-cacert.jks");
			System.setProperty("javax.net.ssl.trustStorePassword", "SD2018");
		}

		Log.setLevel(Level.FINER);

		String ip = IP.hostAddress();
		String serverURI = String.format("https://%s:%s/", ip, NAMENODE_PORT);

		ResourceConfig config = new ResourceConfig();
		int repType = RestBlobStorage.USE_NAMENODE_REPLICATION;
		if (repType == 0)
			config.register(new NamenodeResources());
		else if(repType == 1)
			config.register(new NamenodeResourcesMongo());
		else
			config.register(new ReplicatedNamenodeResources());
		HTTPSAuthenticatingServer server = new HTTPSAuthenticatingServer(SERVER_KEYSTORE, SERVER_KEYSTORE_PWD, SERVER_TRUSTSTORE, SERVER_TRUSTSTORE_PWD, serverURI, config);
		server.start();
		Log.fine(String.format("Namenode Server ready @ %s%s\n", serverURI, Namenode.PATH.substring(1)));

		Discover.me(NAMENODE, serverURI);
	}
}
