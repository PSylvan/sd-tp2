package sys.storage.rest.namenode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;

import api.storage.Namenode;



public class NamenodeResourcesMongo implements Namenode {

	private static final String MONGO = "mongodb://mongo1,mongo2,mongo3/?w=majority&readConcernLevel=majority&readPreference=secondary";
	static final int NAMENODE_PORT = 7777;
	private static Logger logger = Logger.getLogger(Namenode.class.toString());

	MongoCollection<Document> table;
	MongoClient mongo;

	public NamenodeResourcesMongo() {
		logger.fine("Creating a Mongo Namenode");
		MongoClientURI uri = new MongoClientURI(MONGO);
		mongo = new MongoClient(uri);
		MongoDatabase db = mongo.getDatabase("SD");
		table = db.getCollection("namenode");

	}

	@Override
	public Response list(String prefix, int version) {
		logger.fine("Namenode: listing the prefix " + prefix  +" with the version " + version);
		List<String> names = new LinkedList<>();
		List<Document> found = findInDB(prefix).into(new ArrayList<Document>());
		found.forEach(f -> names.add(f.getString("name")));
		return Response.ok( names).build();
	}

	private FindIterable<Document> findInDB(String prefix) {
		Document regQuery = new Document();
		regQuery.append("$regex","^" + Pattern.quote(prefix)  );
		Document findQuery = new Document();
		findQuery.append("name", regQuery);
		return table.find(findQuery);
	}
	
	private boolean existsInDB(String name) {
		Document findQuery = new Document();
		findQuery.put("name", name);
		for(Document d : table.find(findQuery))
			if(d.getString("name").equals(name)) return true;
		return false;
	}

	@Override
	public synchronized Response create(String name, List<String> blocks) {
		logger.fine("Namenode: creating blob " + name  +" with " + blocks.size() + " blocks");
		if(existsInDB(name))
			throw new WebApplicationException(Status.CONFLICT);
		Document document = new Document();
		document.put("name", name);
		document.put("blocks", blocks);
		try {
			table.insertOne(document);
		}catch(Exception e) {
			throw new WebApplicationException(Status.CONFLICT);
		}
		return Response.status(Status.NO_CONTENT).build();
	}

	@Override
	public synchronized Response delete(String prefix) {
		logger.fine("Namenode: deleting the prefix " + prefix);
		Document regQuery = new Document();
		regQuery.append("$regex","^" + Pattern.quote(prefix)  );
		Document findQuery = new Document();
		findQuery.append("name", regQuery);
		DeleteResult deleted = table.deleteMany(findQuery);
		if(deleted.getDeletedCount() == 0L)
			throw new WebApplicationException(Status.NOT_FOUND);
		
		return Response.status(Status.NO_CONTENT).build();
	}

	@Override
	public synchronized Response update(String name, List<String> blocks) {
		logger.fine("Namenode: updating blob " + name  +" with " + blocks.size() + "blocks");
		Document newDoc = new Document();
		newDoc.put("name", name);
		newDoc.put("blocks", blocks);
		if(table.findOneAndUpdate(new Document().append("name", name), newDoc) == null)
			throw new WebApplicationException(Status.NOT_FOUND);
		return Response.status(Status.NO_CONTENT).build();
	}

	@Override
	public Response read(String name, int version) {
		logger.fine("Namenode: Reading blob " + name  +" with the version " + version);
		Document findQuery = new Document();
		findQuery.put("name", name);
		Document doc = table.find(findQuery).first();
		if(doc == null)
			throw new WebApplicationException(Status.NOT_FOUND);
		@SuppressWarnings("unchecked")
		List<String> res = (List<String>) doc.get("blocks");
		return Response.ok(res).build();
	}

}
