package sys.storage.rest.datanode;

import java.io.IOException;
import java.util.concurrent.ExecutionException;


import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import api.storage.Datanode;
import dropbox.CreateFile;
import dropbox.DeleteFile;
import dropbox.DownloadFile;
import dropbox.DropboxClient;
import utils.Random;

public class DatanodeProxyResources extends DropboxClient implements Datanode {

	private static final long MAX_BLOCKS_IN_CACHE = 128;

	protected final String baseURI;

	protected Cache<String, byte[]> blockCache = Caffeine.newBuilder().maximumSize(MAX_BLOCKS_IN_CACHE).build();

	public DatanodeProxyResources(final String baseURI) {
		super();
		this.baseURI = baseURI + Datanode.PATH.substring(1) + "/";
	}

	@Override
	// Second parameter is only used in the GC version...
	public String createBlock(byte[] data, String blob) {
		String id = Random.key128();
		blockCache.put(id, data);
		try {
			CreateFile.execute( DISK_PATH + "/" + id, accessToken, service, data);
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return baseURI.concat(id);
	}

	@Override
	public void deleteBlock(String block) {
		blockCache.invalidate(block);
		try {
			DeleteFile.execute( DISK_PATH + "/" + block, accessToken, service);
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public byte[] readBlock(String block) {
		byte[] data = blockCache.getIfPresent(block);
		if (data == null) {
			try {
				data = DownloadFile.execute( DISK_PATH + "/" + block, accessToken, service);
				
			} catch (InterruptedException | ExecutionException | IOException e) {
				e.printStackTrace();
			}

		}
		return data;
	}

}
