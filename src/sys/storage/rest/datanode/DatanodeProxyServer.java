package sys.storage.rest.datanode;

import static utils.Log.Log;

import java.util.logging.Level;


import org.glassfish.jersey.server.ResourceConfig;

import api.storage.Datanode;
import discovery.Discover;
import sys.storage.rest.RestBlobStorage;
import utils.HTTPSAuthenticatingServer;
import utils.IP;

public class DatanodeProxyServer {
	public static final int DATANODE_PORT = 9999;
	public static final String DATANODE = "Datanode";
	private static final String SERVER_KEYSTORE = "/home/sd/datanode-server-keystore.jks";
	private static final String SERVER_KEYSTORE_PWD = "SD2018";

	private static final String SERVER_TRUSTSTORE = "/home/sd/datanode-truststore-with-cacerts.jks";
	private static final String SERVER_TRUSTSTORE_PWD = "SD2018";

	public static void main(String[] args) throws Exception {
		System.setProperty("java.net.preferIPv4Stack", "true");
		if (RestBlobStorage.LOCAL_TESTING) {
			System.setProperty("javax.net.ssl.keyStore", "./tls/datanode-server-keystore.jks");
			System.setProperty("javax.net.ssl.keyStorePassword", "SD2018");
			System.setProperty("javax.net.ssl.trustStore", "./tls/datanode-truststore-with-cacerts.jks");
			System.setProperty("javax.net.ssl.trustStorePassword", "SD2018");
		}

		Log.setLevel(Level.FINER);

		String ip = IP.hostAddress();
		String serverURI = String.format("https://%s:%s/", ip, DATANODE_PORT);

		ResourceConfig config = new ResourceConfig();
		config.register(new DatanodeProxyResources(serverURI));

		HTTPSAuthenticatingServer server = new HTTPSAuthenticatingServer(SERVER_KEYSTORE, SERVER_KEYSTORE_PWD, SERVER_TRUSTSTORE, SERVER_TRUSTSTORE_PWD, serverURI, config);
		server.start();

		Log.fine(String.format("Datanode Server ready @ %s%s\n", serverURI, Datanode.PATH.substring(1)));

		// Allow the Datanode to be discovered...
		new Thread(() -> Discover.me(DATANODE, serverURI)).start();
	}
}
