package sys.storage.rest;

import static sys.storage.rest.datanode.DatanodeServer.DATANODE;
import static sys.storage.rest.namenode.NamenodeServer.NAMENODE;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import api.storage.BlobStorage;
import api.storage.Datanode;
import api.storage.Namenode;
import discovery.Discover;
import sys.storage.io.BufferedBlobReader;
import sys.storage.io.BufferedBlobWriter;
import sys.storage.rest.Replicated.ReplicatedBlobReader;
import sys.storage.rest.Replicated.ReplicatedBlobWriter;
import utils.Sleep;

public class RestBlobStorage implements BlobStorage {

	private static final int BLOCK_SIZE = 1024;
	public static final boolean USE_KAFKA_FOR_DISCOVERY = true;
	public static final boolean USE_DATANODE_REPLICATION = true;
	public static final int USE_NAMENODE_REPLICATION = 1; // 0 is none, 1 is mongo, 2 is kafka
	public static final byte[] CORRUPTED_BLOCK_RESPONSE = "<<<CORRUPTED BLOCK>>>".getBytes();
	public static final boolean USE_SOAP_HTTPS = true;
	public static final long MAX_NO_COMM_TIME = 7000;
	public static final boolean LOCAL_TESTING = false;
	public static final String DELIMITER = "<>";
	public static final int REPLICATION_FACTOR = 2;
	protected volatile Namenode[] namenodes;
	protected volatile Datanode[] datanodes;
	protected volatile Map<URI, Long> lastCommunicationDatanode;
	protected volatile Map<URI, Long> lastCommunicationNamenode;
	protected BiFunction<String, Integer, Integer> blockWritePolicy;
	private AtomicInteger version;

	final static BiFunction<String, Integer, Integer> SHUFFLE_BLOCKS = (name, block) -> (block + name.hashCode() >>> 1);

	private void setCerts() {
		if (LOCAL_TESTING) {
			System.setProperty("javax.net.ssl.trustStore", "./tls/blobstorage-truststore.jks");
			System.setProperty("javax.net.ssl.trustStorePassword", "SD2018");
		}
	}

	public RestBlobStorage() {
		this(Discover.uriOf(NAMENODE), Discover.uriOf(DATANODE), SHUFFLE_BLOCKS);
		setCerts();
	}

	public RestBlobStorage(BiFunction<String, Integer, Integer> blockWritePolicy) {
		this(Discover.uriOf(NAMENODE), Discover.uriOf(DATANODE), blockWritePolicy);
	}

	protected RestBlobStorage(Namenode namenode, BiFunction<String, Integer, Integer> blockWritePolicy) {
		setCerts();
		this.namenodes = new Namenode[] { namenode };
		this.datanodes = new Datanode[] { new RestDatanodeClient(Discover.uriOf(DATANODE)) };
		this.blockWritePolicy = blockWritePolicy;
		new Thread(this::discoverDatanodes).start();
		new Thread(this::discoverNamenodes).start();
		version = new AtomicInteger(0);
		lastCommunicationDatanode = new ConcurrentHashMap<>();
		lastCommunicationNamenode = new ConcurrentHashMap<>();
		new Thread(this::cleanOldNodes).start();
	}

	private RestBlobStorage(URI namenode, URI datanode, BiFunction<String, Integer, Integer> blockWritePolicy) {
		setCerts();
		this.namenodes = new Namenode[] { new RestNamenodeClient(namenode) };
		this.datanodes = new Datanode[] { new RestDatanodeClient(datanode), };
		this.blockWritePolicy = blockWritePolicy;
		new Thread(this::discoverDatanodes).start();
		new Thread(this::discoverNamenodes).start();
		version = new AtomicInteger(0);
		lastCommunicationDatanode = new ConcurrentHashMap<>();
		lastCommunicationNamenode = new ConcurrentHashMap<>();
		lastCommunicationDatanode.put(datanode, System.currentTimeMillis());
		lastCommunicationNamenode.put(namenode, System.currentTimeMillis());
		new Thread(this::cleanOldNodes).start();
	}

	@Override
	public List<String> listBlobs(String prefix) {
		Response r = null;
		Namenode namenode;
		for (URI uri : lastCommunicationNamenode.keySet()) {
			namenode = new RestNamenodeClient(uri);
			r = namenode.list(prefix, version.get());
			if (r != null)
				break;
		}
		return r.readEntity(new GenericType<List<String>>() {
		});
	}

	@Override
	public void deleteBlobs(String prefix) {
		Response r = null;
		Namenode namenode;
		for (URI uri : lastCommunicationNamenode.keySet()) {
			namenode = new RestNamenodeClient(uri);
			r = namenode.delete(prefix);
			if (r != null)
				break;
		}
	}

	@Override
	public BlobReader readBlob(String name) {
		if (!USE_DATANODE_REPLICATION)
			return new BufferedBlobReader(name, namenodes[0], datanodes[0]);
		else
			return new ReplicatedBlobReader(name, lastCommunicationNamenode, datanodes[0], version);
	}

	@Override
	public BlobWriter blobWriter(String name) {
		if (!USE_DATANODE_REPLICATION)
			return new BufferedBlobWriter(name, namenodes[0], datanodes, BLOCK_SIZE, blockWritePolicy);
		else
			return new ReplicatedBlobWriter(name, lastCommunicationNamenode, lastCommunicationDatanode, BLOCK_SIZE,
					blockWritePolicy, version);

	}

	public void discoverDatanodes() {
		final int RETRY_PERIOD = 100;

		// keep the uris sorted to have consistency of the indexes across instances...
		Set<URI> uris = new TreeSet<>(this::uriComparator);
		for (;;) {
			uris.addAll(Discover.urisOf(DATANODE));
			long time = System.currentTimeMillis();
			// creates a new array of datanode clients (sorted according to their uri).
			uris.forEach(uri -> {
				lastCommunicationDatanode.put(uri, time);
			});

			Sleep.ms(RETRY_PERIOD);
		}
	}

	public void discoverNamenodes() {
		final int RETRY_PERIOD = 100;

		// keep the uris sorted to have consistency of the indexes across instances...
		Set<URI> uris = new TreeSet<>(this::uriComparator);
		for (;;) {
			uris.addAll(Discover.urisOf(NAMENODE));
			long time = System.currentTimeMillis();
			// creates a new array of namenode clients (sorted according to their uri).
			uris.forEach(uri -> {
				lastCommunicationNamenode.put(uri, time);
			});

			Sleep.ms(RETRY_PERIOD);
		}
	}

	// The array of nodes needs to be sorted everywhere to
	// implement consistent mapping of blocks to datanodes based on name
	public int uriComparator(URI a, URI b) {
		return a.getHost().compareTo(b.getHost());
	}

	public void cleanOldNodes() {
		while (true) {
			long currTime = System.currentTimeMillis();
			Set<URI> nodesToRemove = new TreeSet<URI>();
			for (URI u : lastCommunicationDatanode.keySet()) {
				if (currTime - lastCommunicationDatanode.get(u) > MAX_NO_COMM_TIME)
					nodesToRemove.add(u);
			}

			nodesToRemove.forEach(u -> lastCommunicationDatanode.remove(u));

			nodesToRemove = new TreeSet<URI>();
			for (URI u : lastCommunicationNamenode.keySet()) {
				if (currTime - lastCommunicationNamenode.get(u) > MAX_NO_COMM_TIME)
					nodesToRemove.add(u);
			}
			nodesToRemove.forEach(u -> lastCommunicationNamenode.remove(u));
		}
	}
}
