package dropbox;

import java.io.Serializable;

import com.github.scribejava.core.model.OAuth2AccessToken;

public class TokenSerializable implements Serializable{

	private static final long serialVersionUID = 676314324676453249L;
	
	private String accToken;
	private String tokenType;
	
	private Integer expiresIn;
	
	private String refreshToken; 
	private String scope;
	
	public TokenSerializable(String accToken, String tokenType, Integer expiresIn, String refreshToken, String scope) {
	 	this.accToken = accToken;
        this.tokenType = tokenType;
        this.expiresIn = expiresIn;
        this.refreshToken = refreshToken;
        this.scope = scope;
	}
	
	public TokenSerializable(OAuth2AccessToken t) {
	 	this.accToken = t.getAccessToken();
        this.tokenType = t.getTokenType();
        this.expiresIn = t.getExpiresIn();
        this.refreshToken = t.getRefreshToken();
        this.scope = t.getScope();
}
	
	public OAuth2AccessToken generateOAuth2AccessToken() {
		return new OAuth2AccessToken(accToken, tokenType, expiresIn, refreshToken, scope, "");
	}
}
