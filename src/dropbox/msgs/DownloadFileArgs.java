package dropbox.msgs;

public class DownloadFileArgs {
	final String path;
	
	public DownloadFileArgs(String path) {
		this.path = path;
	}
}
