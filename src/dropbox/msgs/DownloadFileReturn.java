package dropbox.msgs;

public class DownloadFileReturn {
	final String size;
	
	public DownloadFileReturn(String size) {
		this.size = size;
	}
}
