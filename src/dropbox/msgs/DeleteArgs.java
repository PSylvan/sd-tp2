package dropbox.msgs;

public class DeleteArgs {
	final String path;
	
	public DeleteArgs( String path) {
		this.path = path;
	}
}
