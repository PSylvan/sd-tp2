package dropbox;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

import dropbox.msgs.DeleteArgs;
import utils.JSON;

public class DeleteFile {

	private static final String DELETE_FILE_V2_URL = "https://api.dropboxapi.com/2/files/delete_v2";

	public static void execute(String path, OAuth2AccessToken accessToken, OAuth20Service service) throws InterruptedException, ExecutionException, IOException {
		System.err.println("EXECUTING DELETE");
		OAuthRequest deleteFile = new OAuthRequest(Verb.POST, DELETE_FILE_V2_URL);
		deleteFile.addHeader("Content-Type", DropboxClient.JSON_CONTENT_TYPE);
		deleteFile.setPayload(JSON.encode(new DeleteArgs(path)));
		service.signRequest(accessToken, deleteFile);
		Response r = service.execute(deleteFile);
		if (r.getCode() == 409) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
	}
}
