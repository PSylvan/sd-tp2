package dropbox;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

import dropbox.msgs.DownloadFileArgs;
import utils.JSON;

public class DownloadFile {
	private static final String DOWNLOAD_FILE_V2_URL = "https://content.dropboxapi.com/2/files/download";

	public static byte[] execute(String path, OAuth2AccessToken accessToken, OAuth20Service service) throws InterruptedException, ExecutionException, IOException {
		OAuthRequest downloadFile = new OAuthRequest(Verb.POST, DOWNLOAD_FILE_V2_URL);
		downloadFile.addHeader("Dropbox-API-Arg", JSON.encode(new DownloadFileArgs(path)));
		downloadFile.addHeader("Content-Type", DropboxClient.OCTET_CONTENT_TYPE);
		service.signRequest(accessToken, downloadFile);
		Response r = service.execute(downloadFile);

		if (r.getCode() == 404) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}

		return r.getBody().getBytes();

	}
}
