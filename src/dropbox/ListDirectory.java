package dropbox;

import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;

import dropbox.msgs.ListFolderContinueV2Args;
import dropbox.msgs.ListFolderV2Args;
import dropbox.msgs.ListFolderV2Return;
import utils.JSON;

public class ListDirectory extends DropboxClient {

	private static final String LIST_FOLDER_V2_URL = "https://api.dropboxapi.com/2/files/list_folder";
	private static final String LIST_FOLDER_CONTINUE_V2_URL = "https://api.dropboxapi.com/2/files/list_folder/continue";
	
	void execute( String path ) throws Exception {
		OAuthRequest listFolder = new OAuthRequest(Verb.POST, LIST_FOLDER_V2_URL);
		listFolder.addHeader("Content-Type", JSON_CONTENT_TYPE);
		listFolder.setPayload( JSON.encode(new ListFolderV2Args(path, true)));
		
		for(;;) {	
			service.signRequest(accessToken, listFolder);	
			Response r = service.execute(listFolder);		
			if (r.getCode() != 200) 
				throw new RuntimeException("Failed: " + r.getMessage() );
			
			ListFolderV2Return result = JSON.decode( r.getBody(), ListFolderV2Return.class);
			result.getEntries().forEach( System.out::println );	

			if(result.has_more()) {
				System.err.println("continuing...");
				listFolder = new OAuthRequest(Verb.POST, LIST_FOLDER_CONTINUE_V2_URL);
				listFolder.addHeader("Content-Type", JSON_CONTENT_TYPE);
				listFolder.setPayload( JSON.encode(new ListFolderContinueV2Args( result.getCursor() )));
			} else
				break;
		}
	}
	
	public static void main(String[] args) throws Exception {
		new ListDirectory().execute("");
	}
}
