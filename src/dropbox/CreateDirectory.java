package dropbox;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

import dropbox.msgs.CreateFolderV2Args;
import utils.JSON;

public class CreateDirectory {

	private static final String CREATE_FOLDER_V2_URL = "https://api.dropboxapi.com/2/files/create_folder_v2";

	public static void execute(String path, OAuth2AccessToken accessToken, OAuth20Service service) throws Exception {
		OAuthRequest createFolder = new OAuthRequest(Verb.POST, CREATE_FOLDER_V2_URL);
		createFolder.addHeader("Content-Type", DropboxClient.JSON_CONTENT_TYPE);

		createFolder.setPayload(JSON.encode(new CreateFolderV2Args(path, false)));

		service.signRequest(accessToken, createFolder);
		Response r = service.execute(createFolder);

		if (r.getCode() == 409) {
			System.err.println("Dropbox directory already exists");
		} else if (r.getCode() == 200) {
			System.err.println("Dropbox directory was created with success");
			return;
		} else {
			System.err.println("Unexpected error HTTP: " + r.getCode());
		}

	}

}
