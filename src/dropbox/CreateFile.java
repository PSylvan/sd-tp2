package dropbox;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

import dropbox.msgs.UploadArgs;
import utils.JSON;

public class CreateFile {
	private static final String MODE_ADD = "add";
	private static final String CREATE_FILE_V2_URL = "https://content.dropboxapi.com/2/files/upload";

	public static void execute(String path, OAuth2AccessToken accessToken, OAuth20Service service, byte[] data) throws InterruptedException, ExecutionException, IOException {
		OAuthRequest createFile = new OAuthRequest(Verb.POST, CREATE_FILE_V2_URL);
		createFile.addHeader("Content-Type", DropboxClient.OCTET_CONTENT_TYPE);
		createFile.addHeader("Dropbox-API-Arg", JSON.encode(new UploadArgs(path, MODE_ADD, false, true)));
		createFile.setPayload(data);
		service.signRequest(accessToken, createFile);

		Response r = service.execute(createFile);
		if (r.getCode() == 409) {
			throw new WebApplicationException(Status.CONFLICT);
		}

	}
}
