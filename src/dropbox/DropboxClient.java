package dropbox;

import java.io.File;
import java.net.URI;
import java.util.Scanner;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.pac4j.scribe.builder.api.DropboxApi20;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;

import utils.IO;

public class DropboxClient {
	private static final String apiKey = "ja1l31kmcrxqogl";
	private static final String apiSecret = "dvzapa8gitwmjy8";

	protected static final String JSON_CONTENT_TYPE = "application/json; charset=utf-8";
	protected static final String OCTET_CONTENT_TYPE = "application/octet-stream";

	protected OAuth20Service service;
	protected OAuth2AccessToken accessToken;

	public DropboxClient() {
		service = new ServiceBuilder().apiKey(apiKey).apiSecret(apiSecret).callback(OAuthCallbackServlet.CALLBACK_URI)
				.build(DropboxApi20.INSTANCE);
		String authorizationID;
		// Props.parseFile(PROPS_FILENAME);
		// authorizationID= Props.get("dropbox-access-token", null);

		File f = new File("/home/sd/accessToken");
		if (f.exists()) {
			String savedAccessToken = new String(IO.read(f));
			accessToken = new OAuth2AccessToken(savedAccessToken);
		} else {
			try {
				OAuthCallbackServlet.start();

				String authorizationURL = service.getAuthorizationUrl();
				System.out.println("Open the following URL in a browser:\n" + authorizationURL);

				Scanner sc = new Scanner(System.in);
				System.out.print("\n\nAuthorization code: ");
				authorizationID = sc.nextLine();
				sc.close();

				File accessTokenFile = new File("./accessToken");
				accessToken = service.getAccessToken(authorizationID);
				IO.write(accessTokenFile, accessToken.getAccessToken().getBytes());
				System.err.println("WROTE: " + accessToken.getAccessToken());
			} catch (Exception x) {
				x.printStackTrace();
				System.exit(0);
			}

		}

	}

	@Path("/")
	public static class OAuthCallbackServlet {
		public static final String CALLBACK_URI = "http://localhost:5555/";

		@GET
		public String callback(@QueryParam("code") String code) {
			return String.format("<html>Authorization-Code: %s</html>", code);
		}

		public static void start() {
			ResourceConfig config = new ResourceConfig();
			config.register(new OAuthCallbackServlet());
			JdkHttpServerFactory.createHttpServer(URI.create(CALLBACK_URI), config);
		}
	}
}
