package api.storage;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(Namenode.PATH)
public interface Namenode {

	static final String PATH="/namenode";
	
	@GET
	@Path("/list/")
	@Produces(MediaType.APPLICATION_JSON)
	Response list( @QueryParam("prefix") String prefix, @DefaultValue("0") @QueryParam("version") int version);

	@GET
	@Path("/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	Response read(@PathParam("name") String name, @DefaultValue("0") @QueryParam("version") int version);

	@POST
	@Path("/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	Response create(@PathParam("name") String name, List<String> blocks);

	@PUT
	@Path("/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	Response update(@PathParam("name") String name, List<String> blocks);

	@DELETE
	@Path("/list/")
	Response delete( @QueryParam("prefix") String prefix);
	
}
