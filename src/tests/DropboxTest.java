package tests;



import api.storage.Datanode;
import discovery.Discover;
import sys.storage.rest.RestDatanodeClient;

public class DropboxTest {
	public static void main(String[] args) {
		
		System.setProperty("java.net.preferIPv4Stack", "true");
		System.setProperty("javax.net.ssl.trustStore", "./tls/blobstorage-truststore.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "SD2018");
		
		Datanode datanodeClient = new RestDatanodeClient(Discover.uriOf("Datanode"));
		String dataS = "teste3 teste3 teste3";
		System.err.println("Wrote: " + dataS);
		String ans = datanodeClient.createBlock(dataS.getBytes(), "");
		String res = new String(datanodeClient.readBlock(ans));
		System.err.println("Read: "+ res);
		System.err.println(dataS.equals(res));
	}
}
