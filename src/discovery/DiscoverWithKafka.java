package discovery;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import utils.Random;
import utils.Sleep;
import utils.Topics;

public class DiscoverWithKafka {

	private static final long DISCOVERY_TIMEOUT = 5000;

	private static final String TOPIC_PREFIX = "Discovery_";

	public static void me(String key, String url) {
		System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "OFF");

		Properties pProps = new Properties();
		pProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka1:9092,kafka2:9092,kafka3:9092");
		pProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
		pProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
		Topics.createTopic(TOPIC_PREFIX+key, 1);
		@SuppressWarnings("resource")
		Producer<String, String> producer = new KafkaProducer<>(pProps);
		while(true) {
			
			producer.send( new ProducerRecord<String, String>(TOPIC_PREFIX + key, url));
			Sleep.ms(500);
		}
	}

	public static Set<URI> search(String serviceWanted) {
		System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "OFF");
		Properties cProps = new Properties();
		cProps.put(ConsumerConfig.GROUP_ID_CONFIG, Random.key64() );
		cProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
		cProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka1:9092,kafka2:9092,kafka3:9092");
		cProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		cProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		
		Set<URI> results = new HashSet<>();

		Consumer<String, String> consumer = new KafkaConsumer<>(cProps);
		consumer.subscribe( Arrays.asList(TOPIC_PREFIX + serviceWanted));// subscrever ao topico "Discovery"
		ConsumerRecords<String, String> records = consumer.poll(DISCOVERY_TIMEOUT);
		records.forEach(r -> {
			String uri = r.value();
			results.add(URI.create(uri));
		});
		consumer.close();
		return results;

	}

	// Does not return until the uri of a given service is found
	public static URI uriOf(String name) {
		return urisOf(name).get(0);
	}

	// Does not return until at least one uri for the given service is found
	public static List<URI> urisOf(String name) {
		Set<URI> uris;
		while ((uris = DiscoverWithKafka.search(name)).isEmpty())
			;
		return new ArrayList<>(uris);
	}
}
